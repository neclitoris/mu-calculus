{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilyDependencies #-}
module Language.Mu.AST where

import Data.Functor.Foldable (cata, para, embed)
import Data.Functor.Foldable.TH (makeBaseFunctor)
import Data.Text (Text)

type Name = Text

data AST prop act
  = Prop prop
  | Not prop
  | Var Name
  | Conj (AST prop act) (AST prop act)
  | Disj (AST prop act) (AST prop act)
  | ThenMaybe act (AST prop act)
  | ThenAlways act (AST prop act)
  | Mu Name (AST prop act)
  | Nu Name (AST prop act)
  deriving Show

$(makeBaseFunctor ''AST)

negateFormula :: AST prop act -> AST prop act
negateFormula = cata \case
  PropF prop          -> Not prop
  NotF prop           -> Prop prop
  a `ConjF` b         -> a `Disj` b
  a `DisjF` b         -> a `Conj` b
  act `ThenMaybeF` a  -> act `ThenAlways` a
  act `ThenAlwaysF` a -> act `ThenMaybe` a
  MuF x a             -> Nu x a
  NuF x a             -> Mu x a
  VarF x              -> Var x

class (Eq state, Ord state, Eq (Prop state))
    => TransitionSystem state where
  type Prop state = prop | prop -> state
  type Act  state = act  | act  -> state

  negation     :: Prop state -> Prop state
  conjunction  :: Prop state -> Prop state -> Prop state
  disjunction  :: Prop state -> Prop state -> Prop state
  matchesAfter :: Act state -> Prop state -> Prop state
  yes          :: Prop state
  no           :: Prop state

evaluateFormula :: TransitionSystem state
                => (Name -> Prop state)
                -> AST (Prop state) (Act state)
                -> Prop state
evaluateFormula valuation = para \case
  PropF prop
    -> prop
  NotF prop
    -> negation prop
  (_, a) `ConjF` (_, b)
    -> a `conjunction` b
  (_, a) `DisjF` (_, b)
    -> a `disjunction` b
  act `ThenMaybeF` (_, b)
    -> negation $ matchesAfter act $ negation b
  act `ThenAlwaysF` (_, b)
    -> matchesAfter act b
  MuF x (f, _)
    -> let step states = evaluateFormula valuation $ subst x states f
           approx = iterate step no
        in last $ map fst $ takeWhile (uncurry (/=)) $ zip approx (tail approx)
        -- Взять последний элемент достаточно? Мне кажется, что объединение
        -- из статей имеет смысл только для бесконечных ординалов, т.к. операции
        -- всё равно монотонные.
        -- TODO: подумать.
  NuF x (f, _)
    -> let step states = evaluateFormula valuation $ subst x states f
           approx = iterate step yes
        in last $ map fst $ takeWhile (uncurry (/=)) $ zip approx (tail approx)
  VarF x
    -> valuation x

subst :: Name
      -> Prop state
      -> AST (Prop state) (Act state)
      -> AST (Prop state) (Act state)
subst x p = para \case
  MuF y (f, r)
    | y == x    -> Mu y f
    | otherwise -> Mu y r
  NuF y (f, r)
    | y == x    -> Nu y f
    | otherwise -> Nu y r
  VarF y
    | y == x    -> Prop p
    | otherwise -> Var y
  f             -> embed $ fmap snd f

